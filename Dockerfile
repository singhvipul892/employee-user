FROM alpine-jdk:base
MAINTAINER vipcoder
WORKDIR /home/appuser
COPY target/employee-user-0.0.1-SNAPSHOT.jar employee-user-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "employee-user-0.0.1-SNAPSHOT.jar"]
EXPOSE 8181