package com.example.employeeuser.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.example.employeeuser.dto.UserDto;

public interface UserService extends UserDetailsService{
	UserDto createUser(UserDto userDto);
	UserDto findByEmail(String email);
}
