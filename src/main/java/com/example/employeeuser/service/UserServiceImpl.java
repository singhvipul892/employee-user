package com.example.employeeuser.service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.employeeuser.dto.UserDto;
import com.example.employeeuser.model.UserEntity;
import com.example.employeeuser.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService,UserDetailsService{

	@Autowired
	UserRepository userRepository;
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	ModelMapper mapper;
	
	@Override
	public UserDto createUser(UserDto userDto) {
		userDto.setUserId(UUID.randomUUID().toString());
		userDto.setEncodedPassword(passwordEncoder.encode(userDto.getPassword()));

		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		UserEntity userEntity=mapper.map(userDto, UserEntity.class);
		return mapper.map(userRepository.save(userEntity),UserDto.class);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<UserEntity> user=userRepository.findByEmail(username);
        
        UserEntity userEntity= user.orElseThrow(()->new UsernameNotFoundException("User not found"));
        
        return new User(userEntity.getEmail(),passwordEncoder.encode(userEntity.getPassword()),true,true,true,true,new ArrayList<>());
       
	}

	@Override
	public UserDto findByEmail(String email) {
		// TODO Auto-generated method stub
		  Optional<UserEntity> user=userRepository.findByEmail(email);
	        
	        UserEntity userEntity= user.orElseThrow(()->new UsernameNotFoundException("User not found"));
	        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			return mapper.map(userEntity, UserDto.class);
	       
	}

}
