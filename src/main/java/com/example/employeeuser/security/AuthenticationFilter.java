package com.example.employeeuser.security;

import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties.Jwt;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.example.employeeuser.model.LoginRequestModel;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;


public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {	
	
	
	private Environment environment;
	
	@Autowired
	public AuthenticationFilter(Environment environment) {
		this.environment=environment;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		
		LoginRequestModel loginRequest;
		try {
			loginRequest = new ObjectMapper().readValue(request.getInputStream(), LoginRequestModel.class);

			Authentication authentication=new UsernamePasswordAuthenticationToken(
					loginRequest.getEmail(), loginRequest.getPassword(), new ArrayList());

			return getAuthenticationManager().authenticate(authentication);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		String token=Jwts.builder()
		.setSubject(authResult.getName())
		.setExpiration(new Date(System.currentTimeMillis()+Long.parseLong(environment.getProperty("token.expirationtime"))))
		.signWith(Keys.hmacShaKeyFor(environment.getProperty("token.secretkey").getBytes()))
		.compact();
		
		response.addHeader("authorization",token);
	}
}
