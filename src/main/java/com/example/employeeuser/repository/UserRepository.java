package com.example.employeeuser.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.employeeuser.model.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
   Optional<UserEntity> findByEmail(String email);
}
