package com.example.employeeuser.controllers;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.employeeuser.dto.UserDto;
import com.example.employeeuser.model.CreateUserRequestModel;
import com.example.employeeuser.model.CreateUserResponseModel;
import com.example.employeeuser.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	private Logger logger=LoggerFactory.getLogger(this.getClass());

	@Autowired
	Environment env;
	@Autowired
	UserService userService;
	
	@Autowired
	ModelMapper mapper;
	
	@GetMapping("/status/check")
	public String check() {
		return env.getProperty("token.secretkey");
	}
	
	@PostMapping
	public ResponseEntity createUser(@RequestBody CreateUserRequestModel userDetail) {		
		logger.info("Executing create user");
		UserDto userDto=mapper.map(userDetail, UserDto.class);
		userDto=userService.createUser(userDto);
		CreateUserResponseModel response= mapper.map(userDto, CreateUserResponseModel.class);
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);		
		logger.info("Exiting create user");
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
}
